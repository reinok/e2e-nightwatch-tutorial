This is published in: https://medium.com/@jaanreinok/e2e-testing-locally-and-in-gitlab-ci-with-nightwatch-js-20b519aa91de
# E2E testing locally and in Gitlab CI with Nightwatch.js

When you want to rise quality of your dev then e2e tests are way to go. Nothing beats manual testing but with e2e tests in Nightwatch.js you can run your main usage flows locally while developing and in CI while deploying. 

Nightwatch.js is an integrated, easy to use End-to-End testing solution for web applications and websites, written in Node.js. It uses the W3C WebDriver API to drive browsers in order to perform commands and assertions on DOM elements.
`ref`: https://nightwatchjs.org/.

Nightwatch is popular framework with over 10k stars in github and its offered as default options for e2e testing in popular FE frameworks like Vue CLI.

In this article it will be described:
* How to write simple test and run it manually.
* How to automate e2e in GitLab CI and possibly in other CI's.
* How to start your e2e tests in CI via Gitlab job api.
  
# Dev dependency

* [node.js](https://nodejs.org/en/download/)
* [yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable) or npm (yarn will be used as default package manager)

    version can be checked with `node -v` and `yarn -v`
    
If you are interested more in how to itegrate e2e test to ci then skip next chapter.    
# Installation and writing first test for local running

The installation of nightwatch is described here: [https://nightwatchjs.org/gettingstarted/installation/](https://nightwatchjs.org/gettingstarted/installation/)

Open your code editor terminal. In this example im using VScode. We will `yarn init` what will create package.json, `yarn add` nightwatch itself, chromedriver for local running and [dotenv](https://classic.yarnpkg.com/en/package/dotenv) for local enviromental variables storing. 

It would be good idea to `git init` to track changes that are made.

```
yarn init

yarn add nightwatch

yarn add chromedriver

yarn add dotenv

```


Commit after these changes: [https://gitlab.com/reinok/e2e-nightwatch-tutorial/](https://gitlab.com/reinok/e2e-nightwatch-tutorial/-/commit/3709a601877abe5a39f014d20d39b02ec72bc0ed)

# Configuration of nightwatch for local running

Create `nightwatch.conf.js` to root. This is basic configuration, what is enough to run tests locally.

```js
const chromedriver = require("chromedriver");

require("dotenv").config();

module.exports = {
  src_folders: ["/tests"],
  page_objects_path: ["tests/pages"],
  test_workers: false,

  webdriver: {
    start_process: true,
    port: 9515,
    server_path: chromedriver.path
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: "chrome",
        chromeOptions: {
        },
      },
    },
  }
};
```
# Lets write basic test using page objects

Read more about Page objects [https://nightwatchjs.org/api/pageobject/](https://nightwatchjs.org/api/pageobject/)

1. Create `test` folder `tests/pages` folders in root:
2. Create `login.js` in `./tests/pages`

```js
//./tests/pages/login.js


var loginCommands = {
  user1: function () {
    return this.login(
      process.env.USER1_LOGIN,
      process.env.USER1_PASSWORD,
      process.env.USER1_NAME,
    );
  },
  login (userName, password, userFullName) {
    this.waitForElementVisible("body")
      .setValue("@userName", userName)
      .setValue("@password", password)
      .saveScreenshot("tests_output/login-before-submit.png")
      .click("@submit")
      .assert.containsText("@fullName", userFullName)
      .saveScreenshot("tests_output/login-after-submit.png")
  }
};
module.exports = {
  commands: [loginCommands],
  url: function () {
    return process.env.URL;
  },
  elements: {
    userName: {
      selector: '[id="username"]'
      // locateStrategy: "css selector" //default
    },
    password: {
      selector: '[id="password"]'
    },
    submit: {
      selector: '[jhitranslate="login.form.button"]'
    },
    fullName: {
      selector: `//*[@jhitranslate="home.greet"]/span[contains(text(), "${process.env.USER1_NAME}")]`,
      locateStrategy: "xpath"
    },
  }
};
```
3. Create `example-flow.js` in `./tests`. Here `navigate()` will use `url: function () {
    return process.env.URL;
  },` from page object what return url specified in env. what will be described
```js
module.exports = {
    '@tags': ['example-flow'],
    'example flow': function (browser) {
        const login = browser.page.login();
        login.navigate().user(browser);
        //additional steps
        browser.end();
    },
};
```
4. Create `main.js` in `./tests`
```js
module.exports = Object.assign(
  {
    before: (browser, done) => {
      // runs before all of the tests run, call done() when you're finished
      done();
    },
    after: (browser, done) => {
      browser.end(); // kill the browser
      done(); // tell nightwatch we're done
    }
  },
  require("./example-flow.js")
);
```
5. Add script for local testing to `package.json`:
```json
{
  "name": "e2e-nightwatch-test-example",
  "version": "1.0.0",
  "description": "Your description",
  "main": "index.js",
  "scripts": {
    "test": "nightwatch -c nightwatch.conf.js",
  },
  "repository": "To be specified",
  "author": "jaanreinok@gmail.com",
  "license": "MIT",
  "dependencies": {
    "chromedriver": "^80.0.1",
    "dotenv": "^8.2.0",
    "nightwatch": "^1.3.4"
  }
}
```
6. Create `.env` in root. Here we will store data that is sensitive like passwords. its good idea to add this to .gitignore so it wont get pushed to repo by mistake.
```
URL="https://www.cityx.ee/portal/#/login"

USER1_LOGIN="demo"
USER1_PASSWORD="demo"
USER1_NAME="Demo holding OÜ"
```

7. Run test via terminal `yarn test .\tests\main.js`. This will should open chrome window and run test visually. For headless run `--headless` can be used in `nightwatch.conf.js` in `chromeOptions: { args: ["--headless] }`. Warning! While running test in headless mode some things might work differently.
The output from terminal should be similar:
```
PS C:\Users\jaanr\source\repos\e2e-testing-article> yarn test .\tests\example-flow.js
yarn run v1.22.4
$ nightwatch .\tests\example-flow.js

[Example Flow] Test Suite
=========================
\ Connecting to localhost on port 9515...

i Connected to localhost on port 9515 (4663ms).
  Using: chrome (80.0.3987.149) on Windows platform.

Running:  exampleFlow

√ Element <body> was visible after 27 milliseconds.
√ Testing if element <Element [name=@fullName]> contains text 'Demo holding OÜ' (539ms)

OK. 2 assertions passed. (2.365s)
Done in 7.86s.
```
Commit after these changes: [https://gitlab.com/reinok/e2e-nightwatch-tutorial/](https://gitlab.com/reinok/e2e-nightwatch-tutorial/-/commit/2e19c496a267849de90a359544f89bbd33868af8)
# Use custom test attributes in you DOM
If you have control of your frontend, over your DOM. Then its good idea to supply elements with specific attributes. For example something like `test-id="login-username"`

In you test you will add something like.
```js
//./tests/your-test.js
const { byTestID} = require("../../util.js");
```
define:
```js
//util.js
module.exports = { byTestID: id => `[test-id="${id}"]`}
```
and use in page objects like:
```js
module.exports = {
  commands: [loginCommands],
  url: function () {
    return process.env.URL;
  },
  elements: {
    sign: {
      selector: byTestID("login-username")
    },
```
# Creating Gitlab CI e2e test runner

Here we have 2 main scenarios. I wont cover deployment part as this can be very different in each case. 

 * In first scenario is where your testable endpoint is public. Using shared runner, what gitlab offers out of the box is enough.
 * Second scenario is when you have locally deployed test env then you have to create your own custom runner.

In this tutorial the e2e tests will be in separate repo to simplify things.

For sake of this tutorial we will describe Gitlabs [triggering pipeline through api](https://docs.gitlab.com/ee/ci/triggers/) to start e2e test when your changes have been deployed. You can trigger e2e test pipeline via curl or powershells Invoke-RestMethod or else.

# Testing public endpoint with Gitlab shared runner
Here were will describe:
 * Create addition nightwatch configuration file for `selenium/standalone-chrome` image what is a [Service](https://docs.gitlab.com/ee/ci/services/) in Gitlab
 * `.gitlab-ci.yml` what is basically configuration file for Gitlab CI
 * Describe how to start a job via pipeline trigger api.

# Creating nightwatch configuration for sunning on existing selenium server
This can be quite tricky and if you don't know what to do and where errors come from.

Create `nightwatch-selenium.conf.js` file in root. Notice that env variable `SELENIUM_HOST` is used and its value will be set in CI configuration described further. `ChromeOptions` are important without them strange errors can appear.
```js
const chromedriver = require("chromedriver");

require("dotenv").config();

module.exports = {
  src_folders: ["/tests"],
  page_objects_path: ["tests/pages"],

  test_workers: false,

  selenium: {
    start_process: false,

    cli_args: {
      "webdriver.chrome.driver": chromedriver.path
    }
  },

  webdriver: {
    start_process: false
  },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: "${SELENIUM_HOST}",

      screenshots: {
        enabled: true,
        path: "tests_output/",
        on_failure: true
      },

      desiredCapabilities: {
        browserName: "chrome",
        chromeOptions: {
          w3c: false,
          args: ["--no-sandbox"]
        }
      }
    }
  }
};

```
# Add command to script in package.json
`-c` vill specify custom configuration file path. This `test-selenium` command will be used in CI configuration.
```
"scripts": {
    "test": "nightwatch",
    "test-selenium": "nightwatch -c nightwatch-selenium.conf.js",
```
# Create .gitlab-ci.yml 

Literally create `.gitlab-ci.yml` in root.
`services:` specifies docker image that runs during a job linked to the Docker image that the image keyword defines. In most cases service images run db containers but we will be running selenium server. 

We specify env variable `SELENIUM_HOST:` what is very important that will give nightwatch address of selenium server. The rest is trivial - installation of dependencies and and running 

artifacts are good way to see screenshots of your tests that run behind the scenes.
```
image: node:12
stages:
  - test
test:
  stage: test
  services:
    - selenium/standalone-chrome
  variables:
    SELENIUM_HOST: selenium__standalone-chrome
  script:
    - yarn
    - yarn test-selenium "/tests/main.js"
  artifacts:
    when: always
    paths:
      - tests_output/
    expire_in: 1 hour
```

# Push to Gitlab and see the results!
Wait before you push. Where the variables come from if the .env is gitignored. No worries Gitlab lets you store project related variables. in gitlab go `setting -> CI/CD -> variables -> expand`. Enter same variables as in your .env file adn save. Now push your changes. At first I got an error: `The engine "node" is incompatible with this module. Expected version ">=10".` So i've changed node to version from 8.10 to 12.
Gitlab handled all the hard work of adding runners workers. In this case shared runner was used. 
The results:
```
 Running with gitlab-runner 12.9.0 (4c96e5ad)
   on docker-auto-scale fa6cab46
Preparing the "docker+machine" executor
01:22
 Using Docker executor with image node:12 ...
 Starting service selenium/standalone-chrome:latest ...
 Pulling docker image selenium/standalone-chrome:latest ...
 ERROR: Preparation failed: Error response from daemon: Get https://registry-1.docker.io/v2/selenium/standalone-chrome/manifests/latest: received unexpected HTTP status: 500 Internal Server Error (docker.go:196:0s)
 Will be retried in 3s ...
 Using Docker executor with image node:12 ...
 Starting service selenium/standalone-chrome:latest ...
 Pulling docker image selenium/standalone-chrome:latest ...
 Using docker image sha256:18ab0431d5ea46d954bd538f6f88bff59d4153839149814f8db4cb54087b153f for selenium/standalone-chrome:latest ...
 Waiting for services to be up and running...
 Pulling docker image node:12 ...
 Using docker image sha256:d834cbcf2402322334c186ef434befd457f97c406d9810fc7a89bb4a57d0dc9a for node:12 ...
Preparing environment
00:05
 Running on runner-fa6cab46-project-17688828-concurrent-0 via runner-fa6cab46-srm-1585150822-3c19f75f...
Getting source from Git repository
00:03
 $ eval "$CI_PRE_CLONE_SCRIPT"
 Fetching changes with git depth set to 50...
 Initialized empty Git repository in /builds/reinok/e2e-nightwatch-tutorial/.git/
 Created fresh repository.
 From https://gitlab.com/reinok/e2e-nightwatch-tutorial
  * [new ref]         refs/pipelines/129663628 -> refs/pipelines/129663628
  * [new branch]      master                   -> origin/master
 Checking out 137f1c07 as master...
 Skipping Git submodules setup
Restoring cache
00:01
Downloading artifacts
00:02
Running before_script and script
00:17
 $ yarn
 yarn install v1.22.0
 [1/4] Resolving packages...
 [2/4] Fetching packages...
 [3/4] Linking dependencies...
 [4/4] Building fresh packages...
 Done in 8.62s.
 $ yarn test-selenium "/tests/main.js"
 yarn run v1.22.0
 $ nightwatch -c nightwatch-selenium.conf.js /tests/main.js
 [Main] Test Suite
 =================
 - Connecting to selenium__standalone-chrome on port 4444...
 ℹ Connected to selenium__standalone-chrome on port 4444 (3450ms).
   Using: chrome (80.0.3987.106) on Linux platform.
 Running:  exampleFlow
 ✔ Element <body> was visible after 62 milliseconds.
 ✔ Testing if element <Element [name=@fullName]> contains text 'Demo holding OÜ' (599ms)
 OK. 2 assertions passed. (3.808s)
 Done in 7.69s.
Running after_script
00:02
Saving cache
00:01
Uploading artifacts for successful job
00:04
 Uploading artifacts...
 tests_output/: found 4 matching files              
 Uploading artifacts to coordinator... ok            id=485393238 responseStatus=201 Created token=PqshiF5P
 Job succeeded
```

Commit after these changes: [https://gitlab.com/reinok/e2e-nightwatch-tutorial/](https://gitlab.com/reinok/e2e-nightwatch-tutorial/-/commit/137f1c07076293bb276b5cd4997f07775f0007e1)

# How to run test on local environment what is not available to shared runner?

For that we have to create GitLab docker runner in the same network. Lets say that your running your test env on windows machine what is not the best scenario for docker usage but no shortcomings in this case. 

Its rather well documented how to install docker runner in [Gitlab](https://docs.gitlab.com/runner/install/docker.html)


Wait this have to be edited for your needs :
`docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest`

You can to edit docker volume `/srv/gitlab-runner/config:` path what will be the place where your config.toml file will be located. I've changed mine to `C:/Users/Public/gitlab-runner`

Now the tip is not to use `locahost`. Use your .env URL ip like 192.168.x.x. And add `-p 80:80` (your app port) to docker run command.

# Editing config.toml file
In your docker volume path set in docker run command what was for me `C:/Users/Public/gitlab-runner` you can find `config.toml`. Ive added `dns_search = ["host.docker.internal"]`
Contents on mine:
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "NAME_YOU_SPECIFIED"
  url = "https://gitlab.com/"
  token = "YOUR_TOKEN"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.1"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
	dns_search = ["host.docker.internal"]

```
# Trigger pipeline job via api
Gitlabs offers [triggering pipeline through api](https://docs.gitlab.com/ee/ci/triggers/) to start e2e test when your changes have been deployed. You can trigger e2e test pipeline via curl or powershells Invoke-RestMethod or else. here I will describe usage of powershell as curl is described in gitlab page. `Settings -> CI/CD -> Pipeline triggers` You can get your project trigger token from there. Project ID you can get from project overview.

This can be used in your deploy script if your deployment process is decoupled from building.

```ps
$body = @{
    token='YOUR_JOB_TOKEN'
    ref='master'
}

Invoke-RestMethod -Uri https://gitlab.com/api/v4/projects/YOUR_PROJECT_ID/trigger/pipeline -Method POST -Body $body
```
# Feedback

Please give me feedback and if you like the article or its been useful to you press the button ;)

# To add

* How to debug tests.
* How to set up selenium standalone server locally to test your CI configuration.
